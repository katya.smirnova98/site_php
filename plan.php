<!DOCTYPE html>
<html lang="uk">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style.css">
    <title>Навчальний план</title>
  </head>
  <body>
  <header>
	 <div class="section">
       <h1><span>Мій блог</span></h1>
     </div>
     <ul class="menu-main">
       <?php require_once ("menu.php");?>
     </ul>
  </header>
        <legend>Навчальний план</legend>
            <table>
		     <tr>
			  <th><b>Предмети:</b></th>
			  <th>Основи WEB програмування</th>
       		  <th>Правознавство</th>
			  <th>Теорія автоматичного керування</th>
	   		  <th>Системи проектування, ідентифікації та моделювання</th>
	   		  <th>Електроніка та мікропроцесорна техніка</th>
	   		  <th>Числові методи і моделювання на ЕОМ</th>
			  <th>Автоматизація технологічних процесів</th>
	  		 </tr>
              <td>Години:</td>
              <td>30</td>
              <td>20</td>
              <td>100</td>
              <td>100</td>
              <td>90</td>
              <td>70</td>
              <td>90</td>
	  		 </tr>
	  		 <tr>
              <td>Лекційні години:</td>
              <td>30</td>
              <td>20</td>
              <td>100</td>
              <td>100</td>
              <td>90</td>
              <td>70</td>
              <td>90</td>
	 		 </tr>
	  		 <tr>
              <td>Практичні години:</td>
              <td>30</td>
              <td>20</td>
              <td>100</td>
              <td>100</td>
              <td>90</td>
              <td>70</td>
              <td>90</td>
	  		 </tr>
	  		 <tr>
              <td>Лабораторні години:</td>
              <td>30</td>
              <td>20</td>
              <td>100</td>
              <td>100</td>
              <td>90</td>
              <td>70</td>
              <td>90</td>
             </tr>
			 <tr>
              <td>Кредити:</td>
              <td>30</td>
              <td>20</td>
              <td>100</td>
              <td>100</td>
              <td>90</td>
              <td>70</td>
              <td>90</td>
             </tr>
	 		</table>
	 <hr/>
     <footer>&copy; 2020 Мій блог. Усі права захищені. <a href="mailto:katya.smirnova98@gmail.com">katya.smirnova98@gmail.com</a></footer>
  </body>
</html>
