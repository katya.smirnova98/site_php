<!DOCTYPE html>
<html lang="uk">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style.css">
    <title>Головна</title>
  </head>
  <body>
  <header>
	 <div class="section">
       <h1><span>Мій блог</span></h1>
     </div>
     <ul class="menu-main">
       <?php require_once ("menu.php");?>
     </ul>
   </header>
     <div id="content" class="section">
	   <img src="photo.jpg" alt="Фото автора"/>
	   <p>Усім привіт! Мене звати Смірнова Катерина. Мені 20 років. Народилась та проживаю у мальовничому м.Черкаси. Деякий період свого життя
	   провела у м. Київ. Наразі я студентка 3-го курсу факультету інформаційно-освітніх технологій Черкаського національного університету 
       ім.Б.Хмельницького. У своєму блозі трохи розповім вам про своє життя, а також поділюсь цікавою та корисною інформацією зі сфери мого навчання.</p>
	 </div>
	 <hr/>
     <footer>&copy; 2020 Мій блог. Усі права захищені. <a href="mailto:katya.smirnova98@gmail.com">katya.smirnova98@gmail.com</a></footer>
  </body>
</html>