<!DOCTYPE html>
<html lang="uk">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style.css">
    <title>Університет</title>
  </head>
  <body>
  <header>
	 <div class="section">
       <h1><span>Мій блог</span></h1>
     </div>
     <ul class="menu-main">
       <?php require_once ("menu.php");?>
     </ul>
   </header>
     <div id="content1" class="section">
	 <img src="university.jpg" alt="Фото ЧНУ"/>
	    <p>Черкаський національний університет імені Богдана Хмельницького — освітній, науковий, культурний та інтелектуальний центр Шевченкового 
		краю, в якому здобувають освіту не тільки українці, а й студенти з різних країн світу.<br/>Для всіх бажаючих отримату освіту в ЧНУ та просто 
		для, тих кому цікаво подивитись, де я навчаюсь, залишаю посилання <a href="http://cdu.edu.ua/" target="_blank">ЧНУ</a>.</p>
	 </div>
	 <hr/>
     <footer>&copy; 2020 Мій блог. Усі права захищені. <a href="mailto:katya.smirnova98@gmail.com">katya.smirnova98@gmail.com</a></footer>
  </body>
</html>